��    (      \  5   �      p     q  	   �     �     �     �     �     �     �  
   �     �     �     	  
             (     ;     A  	   J     T     [     n     {     �     �     �     �     �     �     �  
                  &  
   ,     7     D  	   Y     c     o  #  t     �  
   �     �     �     �     �     �     �            %   2     X     a     m     v     �  	   �  
   �     �     �     �     �     �     	     "	     1	     A	     W	  !   m	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     '                  %                          	   #       !                 $                   &                    
                  (                   "                        Ascending Order Category: Descending Order Download Download Count Home Last 30 Days Last 7 Days Last Month Last Updated Link Template for Archive Page Loading Loading... Looking For No download found! Order Order By Order By: Order: Package Categories Package Size Package Size In Bytes Publish Date Search By Keyword Search Package Search Result For Search Result Page Select Order By Select category or search This Month Title Title: Today View Count View Details View all posts in %s Yesterday in category year Project-Id-Version: Download Manager Pro v3.2.2
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-09-16 15:41+0600
PO-Revision-Date: 2017-09-16 15:41+0600
Last-Translator: Alvaro <alvaro@sobretodopersonas.org>
Language-Team: NexusFireMan <alvaro@sobretodopersonas.org>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: __;_e
X-Poedit-Basepath: ..
X-Poedit-SourceCharset: UTF-8
X-Generator: Poedit 2.0.3
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SearchPath-0: .
 Orden ascendente Categoría Orden descendente Descarga Cantidad de descargas Inicio Últimos 30 días Últimos 7 días Último mes Última actualización Template de enlaces para Archive Page Cargando Cargando... Buscando No se encontraron descargas Orden Orden por Orden por: Orden: Categoría de paquete Tamaño de paquete Tamaño de paquete en Bytes Fecha de publicación Buscar por palabra Buscar paquete Resultados para Página de resultados Seleccionar orden por Selecionar categoría o búsqueda Este mes Título Título: Hoy Vistas Ver detalles Ver todas las entradas en %s Ayer en categoría año 