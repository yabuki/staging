<?php
$image_class = $this->getImageClass( $atts[ 'size' ] );
?>
<div class="yyi-rinker-contents yyi-rinker-postid-<?php echo esc_attr( $post_id )?> <?php echo esc_attr( $image_class ) ?> <?php foreach($category_classes AS $category_class ) { echo esc_attr( $category_class ) . ' '; } ?>">
	<div class="yyi-rinker-box">
		<div class="yyi-rinker-image">
			<a href="<?php echo esc_url( $this->array_get( $meta_datas, self::FREE_TITLE_URL_COLUMN ) ); ?>" target="_blank" rel="nofollow" class="yyi-rinker-tracking" data-click-tracking=""><img src="<?php echo esc_url( $meta_datas[ 'image_url' ]); ?>" class="yyi-rinker-main-img" style="border: none;"></a>
		</div>
		<div class="yyi-rinker-info">
			<div class="yyi-rinker-title">
				<div class="yyi-rinker-title">
					<a rel="nofollow" href="<?php echo esc_url( $this->array_get( $meta_datas, self::FREE_TITLE_URL_COLUMN ) ) ?>" target="_blank" class="yyi-rinker-tracking" data-click-tracking="freelink <?php echo esc_attr( $post_id ) ?> <?php echo esc_attr($this->array_get( $meta_datas, self::TITLE_COLUMN ) ) ?>"><?php echo esc_html($this->array_get( $meta_datas, self::TITLE_COLUMN ) ) ?></a>
				</div>
			</div>
			<div class="yyi-rinker-detail">
			<?php if ( isset( $credit) ) { ?>
				<div class="credit"><?php echo $credit ?></div>
			<?php } ?>
			<?php if ( strlen( $meta_datas[ 'brand' ] ) > 0 ) { ?>
				<div class="brand"><?php echo esc_html( $meta_datas[ 'brand' ] ); ?></div>
			<?php } ?>
				<div class="price-box">
			<?php if ( strlen( $meta_datas[ 'price' ] ) > 0 && intval( $meta_datas[ 'price' ] ) > 0) { ?>
				<span class="price"><?php echo esc_html( '¥' . number_format( intval( $meta_datas[ 'price' ] ) ) ); ?></span>
				<?php if ( strlen( $meta_datas[ 'price_at' ] ) > 0 ) { ?>
					<span class="price_at"><?php echo esc_html( $meta_datas[ 'price_at' ] ); ?></span>
				<?php } ?>
			<?php } ?>
				</div>
			<?php if( isset( $meta_datas[ self::FREE_COMMENT_COLUMN ] ) && strlen( $meta_datas[ self::FREE_COMMENT_COLUMN ] ) > 0 ) { ?>
				<div class="free-text">
					<?php echo wp_kses_post( $meta_datas[ self::FREE_COMMENT_COLUMN ] ) ?>
				</div>
			<?php } ?>
			</div>

			<ul class="yyi-rinker-links">
				<?php if( isset( $meta_datas[ self::FREE_URL_1_COLUMN ] ) &&  strlen( $meta_datas[ self::FREE_URL_1_COLUMN ] ) > 0 ) { ?>
					<li class="freelink1">
						<?php echo ($meta_datas[ self::FREE_URL_1_COLUMN ]) ?>
					</li>
				<?php } ?>
				<?php if( isset( $meta_datas[ self::FREE_URL_3_COLUMN ] ) &&  strlen( $meta_datas[ self::FREE_URL_3_COLUMN ] ) > 0 ) { ?>
					<li class="freelink3">
						<?php echo ($meta_datas[ self::FREE_URL_3_COLUMN ]) ?>
					</li>
				<?php } ?>
				<?php if ( isset( $meta_datas[ self::FREE_URL_2_COLUMN ] ) &&  strlen( $meta_datas[  self::FREE_URL_2_COLUMN ] ) > 0 ) { ?>
					<li class="freelink2">
						<?php echo $meta_datas[ self::FREE_URL_2_COLUMN ] ?>
					</li>
				<?php } ?>
				<?php if( isset( $meta_datas[ self::FREE_URL_4_COLUMN ] ) &&  strlen( $meta_datas[ self::FREE_URL_4_COLUMN ] ) > 0 ) { ?>
					<li class="freelink4">
						<?php echo ($meta_datas[ self::FREE_URL_4_COLUMN ]) ?>
					</li>
				<?php } ?>
			</ul>
		</div>
	</div>
</div>
