<?php
/**
 * The template for displaying all single histry posts.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
get_header();
$container   = get_theme_mod( 'understrap_container_type' );
?>

<div class="wrapper" id="single-glossary-wrapper">

  <div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

    <div class="row single-glossarypage">
      <?php get_template_part('parts/breadcrumbs'); ?>

      <?php get_template_part( 'global-templates/left-sidebar-check' ); ?>

      <main class="site-main" id="main">

        <?php while ( have_posts() ) : the_post(); ?>

        <?php get_template_part( 'loop-templates/content', 'glossary' ); ?>

        <?php //get_template_part('parts/related'); ?>
        <?php get_template_part('parts/adsense'); ?>


        <div class="mt-5  mb-5 border-top">
          <?php get_template_part('parts/nav', 'postlink'); ?>
        </div>

        <?php endwhile; // end of the loop. ?>

      </main><!-- #main -->

      <?php get_template_part( 'global-templates/right-sidebar-check' ); ?>

    </div><!-- .row -->

  </div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer();