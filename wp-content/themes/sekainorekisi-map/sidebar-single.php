<?php
/**
 * The right sidebar containing the main widget area.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

?>

<?php dynamic_sidebar( 'adsense-sidebar' ); ?>

<?php if ( in_category( array( 'movie', 'dorama' ))){
  get_template_part('parts/widget', 'movie');
}
?>

<?php dynamic_sidebar( 'right-sidebar' ); ?>

<?php get_template_part('parts/widget','info'); ?>
