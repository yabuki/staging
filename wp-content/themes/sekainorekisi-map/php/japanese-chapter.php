<?php
            $chap = get_query_var( 'chapter' ); //指定したいタクソノミーを指定
            $args = array(
                'post_type' => array('japanese-history'), /* 投稿タイプを指定 */
                'tax_query' => array(
                    'relation' => 'OR',
                    array(
                        'taxonomy' => 'chapter', /* 指定したい投稿タイプが持つタクソノミーを指定 */
                        'field' => 'slug',
                        'terms' => $chap, /* 上記で指定した変数を指定 */
                    ),
                ),
                'paged' => $paged,
                'posts_per_page' => '5' /* 5件を取得 */
            ); ?>
            <?php query_posts( $args ); ?>
            <?php if (have_posts()) : ?>
            <?php while (have_posts()) : the_post(); /* ループ開始 */ ?>

            <?php the_title(); ?>

            <?php endwhile; else: ?>
            <p><?php echo "お探しの記事、ページは見つかりませんでした。"; ?></p>
            <?php endif; ?>