<?php
$args = array(
  'post_type' => array('glossary', 'post'),
  'posts_per_page' => -1,
  'tax_query' => array(
    array(
      'taxonomy' => 'history_tag',
      'field' => 'slug',
      'terms' => 'kouga_c',
      )
    )
  );
  ?>
<?php $loop = new WP_Query($args); ?>
<ul>
	<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
	<li>
		<a class="place_title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
	</li>
	<?php endwhile; ?>
</ul>
