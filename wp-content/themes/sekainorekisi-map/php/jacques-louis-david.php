<?php
$args = array(
  'post_type' => array('glossary', 'post'),
  'posts_per_page' => -1,
  'tax_query' => array(
    array(
      'taxonomy' => 'history_tag',
      'field' => 'slug',
      'terms' => 'jacques-louis-david',
      )
    )
  );
  ?>
<?php $loop = new WP_Query($args); ?>
<div class="col s12">
	<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
	<div class="col s6">
		<a class="place_title" href="<?php the_permalink(); ?>">
			<h4><?php the_title(); ?></h4>
			<?php the_post_thumbnail('card-search-thumbnail'); ?>
		</a>
	</div>
	<?php endwhile; ?>
</div>
