<?php
$args = array(
  'post_type' => array('glossary', 'post'),
  'posts_per_page' => -1,
  'tax_query' => array(
    array(
      'taxonomy' => 'history_tag',
      'field' => 'slug',
      'terms' => 'johannes-vermeer',
      )
    )
  );
  ?>
<?php $loop = new WP_Query($args); ?>
<div class="row">
	<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
	<div class="col-6 mb-3">
		<a class="place_title" href="<?php the_permalink(); ?>">
			<?php the_post_thumbnail('thumbnail'); ?><br>
			<i class="fa fa-arrow-right" aria-hidden="true"></i> <?php the_title(); ?>
		</a>
	</div>
	<?php endwhile; ?>
</div>
