<div class="mokuji">
	<div class="row">
			<?php // WordPress タームの親・子・孫の一覧に孫タームの投稿一覧を表示する方法
				$taxonomy_slug = 'chapter'; // タクソノミースラッグを指定
				$post_type_slug = 'world-history'; // ポストタイプの指定
				$post_type_slug2 = 'japanese-history';
				$parents = get_terms($taxonomy_slug,'parent=0'); // 親のいないタームを取り出します、つまり親
			foreach ( $parents as $parent ) { // 親タームのループを開始
			echo '<div class="col-md-6"><h2>' . esc_html($parent->name) . '</h2>'; // 親タームのタイトルを表示

			$children = get_terms($taxonomy_slug,'hierarchical=0&parent='.$parent->term_id);
			foreach ( $children as $child ) { // 子タームのループを開始
			echo '<h3>' . esc_html($child->name) . '</h3>'; // 子タームのタイトルを表示

			$grandchildren = get_terms($taxonomy_slug,'hierarchical=0&parent='.$child->term_id);
			foreach ( $grandchildren  as $grandson ) { // 子タームのループを開始
			echo '<h4>' . esc_html($grandson->name) . '</h4>'; // 孫タームのタイトルを表示

			$term_slug = $grandson->slug; // 以下孫タームに紐づく投稿一覧のクエリを設定

			$args = array( // クエリの作成
				'post_type' => array($post_type_slug, $post_type_slug2),// ポストタイプを指定
				$taxonomy_slug => $term_slug , // タクソノミーにタームを指定
				'post_status' => 'publish', // 公開された投稿を指定
				'posts_per_page' => -1, // 条件に当てはまる投稿を全て表示
				'orderby' => 'ID',
				'order' => 'ASC',
			);
			$myquery = new WP_Query( $args ); // クエリのセット
			?>
			<?php if ( $myquery->have_posts()): ?>
				<ul>
					<?php while($myquery->have_posts()): $myquery->the_post(); ?>
					<li><a href="<?php the_permalink() ?>" target="_blank"><?php the_title(); ?></a></li>
					<?php endwhile; ?>
				</ul>
			<?php endif; ?>
			<?php wp_reset_postdata(); ?>
			<?php } // 孫ターム終了 ?>
			<?php } // 子ターム終了 ?>
	</div>
	<?php } // 親ターム終了 ?>
</div>
