<?php
/**
 * The template for displaying all single histry posts.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
get_header();
$container   = get_theme_mod( 'understrap_container_type' );
?>

<div class="wrapper" id="single-world-wrapper">

  <div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

    <div class="row single-world-history">
      <?php get_template_part('parts/breadcrumbs'); ?>

      <?php get_template_part( 'global-templates/left-sidebar-check' ); ?>

      <main class="site-main" id="main">

        <?php while ( have_posts() ) : the_post(); ?>

        <?php get_template_part( 'loop-templates/content', 'single-history' ); ?>

        <?php get_template_part('parts/adsense'); ?>

        <?php get_template_part('parts/nav', 'chapter'); ?>

        <?php endwhile; // end of the loop. ?>

      </main><!-- #main -->

      <?php get_template_part( 'global-templates/right-sidebar-check' ); ?>

    </div><!-- .row -->

  </div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer();
