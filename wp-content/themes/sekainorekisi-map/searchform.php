<?php
/**
 * The template for displaying search forms in Underscores.me
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit
?>

<form method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search">
  <label class="sr-only" for="s"><?php esc_html_e( 'Search', 'sekainorekisi-map' ); ?></label>
  <div class="input-group input-group-sm">
    <input class="field form-control" id="s" name="s" type="text" placeholder="<?php esc_attr_e( 'Search &hellip;', 'sekainorekisi-map' ); ?>" value="<?php the_search_query(); ?>">
    <span class="input-group-append">
      <input class="submit btn btn-info fa-input" id="searchsubmit" name="submit" type="submit" value="&#xf002;">
    </span>
  </div>
</form>
