<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<div class="wrapper" id="error-404-wrapper">

  <div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

    <div class="row">
      <?php get_template_part('parts/breadcrumbs'); ?>

      <?php get_template_part( 'sidebar-templates/sidebar', 'left' ); ?>

      <main class="site-main" id="main">

        <section class="error-404 not-found">

          <header class="page-header">

            <h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.',
							'sekainorekisi-map' ); ?></h1>

          </header><!-- .page-header -->

          <div class="page-content">

            <p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?',
							'sekainorekisi-map' ); ?></p>

            <div class="static-hero-widget bg-info p-3">

              <?php echo do_shortcode('[searchandfilter id="5797"]') ?>

            </div>

          </div><!-- .page-content -->

        </section><!-- .error-404 -->

      </main><!-- #main -->

      <?php get_template_part( 'sidebar-templates/sidebar', 'right' ); ?>

    </div><!-- .row -->

  </div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer();
