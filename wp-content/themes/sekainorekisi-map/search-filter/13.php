<?php
/**
 * Search & Filter Pro
 *
 * Sample Resdivts Template
 *
 * @package   Search_Filter
 * @author    Ross Morsali
 * @link      http://www.designsandcode.com/
 * @copyright 2014 Designs & Code
 *
 * Note: these templates are not fdivl page templates, rather
 * just an encaspdivation of the your resdivts loop which shodivd
 * be inserted in to other pages by using a shortcode - think
 * of it as a template part
 *
 * This template is an absolute base example showing you what
 * you can do, for more customisation see the WordPress docs
 * and using template tags -
 *
 * http://codex.wordpress.org/Template_Tags
 * homepage
 */

if ( $query->have_posts() )
{
  ?>
<div class="mb-5 text-center">
  <img src="https://placehold.jp/728x90.png">
</div>
<div class="sf-pagenation">
  <?php next_posts_link( '<i class="fa fa-arrow-circle-left fa-2x" aria-hidden="true"></i>', $query->max_num_pages ); ?><?php previous_posts_link( '<i class="fa fa-arrow-circle-right fa-2x" aria-hidden="true"></i>'); ?>
  <span class="px-1">検索結果 <?php echo $query->found_posts; ?> 件 <span class="text-secondary">Page <?php echo $query->query['paged']; ?> / <?php echo $query->max_num_pages; ?></span></span>
</div>

<div id="container">
  <div class="card-columns no-gutter-card-dec">

    <?php 
      while ($query->have_posts())
      {
        $query->the_post();
          ?>

    <?php get_template_part('parts/card','archive'); ?>
    <?php
      }
          ?>
  </div>
</div>

<div class="sf-pagenation">
  <?php next_posts_link( '<i class="fa fa-arrow-circle-left fa-2x" aria-hidden="true"></i>', $query->max_num_pages ); ?><?php previous_posts_link( '<i class="fa fa-arrow-circle-right fa-2x" aria-hidden="true"></i>'); ?>
  <span class="px-1">検索結果 <?php echo $query->found_posts; ?> 件 <span class="text-secondary">Page <?php echo $query->query['paged']; ?> / <?php echo $query->max_num_pages; ?></span></span>
</div>

<?php
} else {
  ?>
<?php get_template_part( 'loop-templates/content', 'none' ); ?>
<?php
}
?>
