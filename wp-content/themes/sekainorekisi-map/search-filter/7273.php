<?php
/**
 * Search & Filter Pro
 *
 * Sample Results Template
 *
 * @package   Search_Filter
 * @author    Ross Morsali
 * @link      http://www.designsandcode.com/
 * @copyright 2015 Designs & Code
 *
 * Note: these templates are not full page templates, rather
 * just an encaspulation of the your results loop which should
 * be inserted in to other pages by using a shortcode - think
 * of it as a template part
 *
 * This template is an absolute base example showing you what
 * you can do, for more customisation see the WordPress docs
 * and using template tags -
 *
 * http://codex.wordpress.org/Template_Tags
 * Timeline Timeline Timeline TimelineTimeline TimelineTimeline TimelineTimeline TimelineTimeline Timeline
 */
if ( $query->have_posts() )
{
  ?>
  
<div class="d-block d-sm-none col-12">
  <?php dynamic_sidebar( 'timeline-sidebar' ); ?>
</div>

<div class="sf-pagenation">
  <?php next_posts_link( '<i class="fa fa-arrow-circle-left fa-2x" aria-hidden="true"></i>', $query->max_num_pages ); ?><?php previous_posts_link( '<i class="fa fa-arrow-circle-right fa-2x" aria-hidden="true"></i>'); ?>
  <span class="px-1">検索結果 <?php echo $query->found_posts; ?> 件 <span class="text-secondary">Page <?php echo $query->query['paged']; ?> / <?php echo $query->max_num_pages; ?></span></span>
</div>
    
  <div class="row">
    <div class="timeline-page col s12">
      <ul id="timeline">
        <?php
        while ($query->have_posts())
        {
          $query->the_post();
          ?>
          <li class="work">
            <input class='radio' name="works" type='radio' id="<?php the_ID(); ?>" value="" />
            <div class="relative">
              <label for='<?php the_ID(); ?>'><?php the_title(); ?></label>
              <span class='date'><?php echo post_custom('wpcf-start-era'); ?> <?php echo post_custom('wpcf-start-year'); ?></span>
              <span class='circle'></span>
            </div>
            <div class='content'>
              <div class="content-inner">
               <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" target="_blank">
                <div class="featured-thumbnail">
                  <?php if ( has_post_thumbnail() ) { ?>
                  
                    <?php the_post_thumbnail( 'tooltip-thumbnail', array( 'class' => 'saturate-img' )); ?>
                  
                  <?php
                }
                ?>
              </div>
              <div class="timeline-excerpt">
                  <?php
                  the_content('MORE');
                  ?>
              </div>
              </a>
            </div>
          </div>
        </li>

        <?php
      }
      ?>
    </ul>
  </div>
</div>

<div class="sf-pagenation">
  <?php next_posts_link( '<i class="fa fa-arrow-circle-left fa-2x" aria-hidden="true"></i>', $query->max_num_pages ); ?><?php previous_posts_link( '<i class="fa fa-arrow-circle-right fa-2x" aria-hidden="true"></i>'); ?>
  <span class="px-1">検索結果 <?php echo $query->found_posts; ?> 件 <span class="text-secondary">Page <?php echo $query->query['paged']; ?> / <?php echo $query->max_num_pages; ?></span></span>
</div>

<?php
/* example code for using the wp_pagenavi plugin */
if (function_exists('wp_pagenavi'))
{
  echo "<br />";
  wp_pagenavi( array( 'query' => $query ) );
}
?>

<?php
}
else
{
  echo "No Results Found";
}
?>