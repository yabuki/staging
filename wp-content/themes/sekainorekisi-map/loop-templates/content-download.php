<?php
/**
 * Single post partial template.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

  <?php get_template_part('parts/single', 'jumbotron-download'); ?>

  <div class="entry-content content-single">

    <?php if(is_singular('glossary')) {
      get_template_part('parts/nav', 'era');
      } 
    ?>

    <?php the_content(); ?>

    <?php
		wp_link_pages( array(
			'before' => '<div class="page-links">' . __( 'Pages:', 'sekainorekisi-map' ),
			'after'  => '</div>',
		) );
		?>

  </div><!-- .entry-content -->

  <!--	<footer class="entry-footer">-->

  <?php //understrap_entry_footer(); ?>

  <!--	</footer> .entry-footer -->

</article><!-- #post-## -->
