<?php
/**
 * Post rendering content according to caller of get_template_part.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

<article <?php post_class('card macy-card'); ?>>
	<?php if ( has_post_thumbnail() ) { 
      echo '<a href="';
      echo the_permalink();
      echo '" title="';
      echo the_title_attribute();
      echo '">';
      echo the_post_thumbnail('medium', array( 'class' => 'macy-card-img-top' ));
      echo '</a>';
  
      } else {
      
			echo '<a href="';
      the_permalink();
      echo '" title="';
      the_title_attribute();
      echo '"><img src="';
      echo get_stylesheet_directory_uri() . '/images/no-image-small-c.jpg" alt="" class="macy-card-img-top"></a>';
      }
    ?>

	<?php if (get_post_type() === 'glossary') {
      echo '<span class="post-type">';
      echo '用語集</span>';

      } elseif (get_post_type() === 'post') {
      echo '<span class="post-type">';
      echo '記事</span>';

      } elseif (get_post_type() === 'chronology') {
      echo '<span class="post-type">';
      echo '年表</span>';

      } elseif (get_post_type() === 'japanese-history') {
      echo '<span class="post-type">';
      echo '日本史</span>';

      } elseif (get_post_type() === 'world-history') {
      echo '<span class="post-type">';
      echo '世界史</span>';

	      } elseif (get_post_type() === 'wpdmpro') {
      echo '<span class="post-type">';
      echo 'Download</span>';
	
      } else {
      echo '<span class="post-type">';
      echo 'その他</span>';
      }
    ?>

	<div class="text-hide">
		<time class="entry-date updated" datetime="<?= get_post_time('c', true); ?>" itemprop="dateModified"><?= the_modified_date(); ?></time> , by <span class="vcard author"><span class="fn"><?php the_author(); ?></span></span>
	</div>

	<div class="card-body">

		<?php 
		if(has_term('','chapter')){ 
				echo '<i class="fa fa-folder-o" aria-hidden="true"></i> ';
				the_terms( $post->ID, 'chapter', '', ' &gt; ' );
			
		 } elseif (has_term('','glossary')) {
		      echo '<i class="fa fa-folder-o" aria-hidden="true"></i> ';
      		the_terms( $post->ID, 'glossary', '', '/' );
			
			} elseif (get_post_type() === 'chronology') {
								echo '<i class="tiny material-icons">folder_open</i> 年表';
			
			} elseif (get_post_type() === 'wpdmpro') {
					echo '<i class="tiny material-icons">folder_open</i> ';
			the_terms( $post->ID, 'wpdmcategory', '', '/' );
			
			} else {
					echo '<i class="fa fa-folder-o" aria-hidden="true"></i> <span itemprop="genre">';
					the_category(' &gt; ');
      		echo '</span>';	
		} ?>

		<?php if(post_custom('wpcf-start-year')): ?>
		<i class="fa fa-history" aria-hidden="true"></i> <?php echo post_custom('wpcf-start-era'); ?><?php echo post_custom('wpcf-start-year'); ?>
		<?php endif; ?>

		<?php get_template_part('parts/entry','meta-flag'); ?>

		<div class="text-hide">
			<time class="entry-date updated" datetime="<?= get_post_time('c', true); ?>" itemprop="dateModified"><span class="margin-right05">最終更新日</span><?= the_modified_date(); ?></time>
		</div>
	</div>
	<div class="macy-card-footer">
		<h2 class="macy-card-title">
			<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
				<?php the_title(); ?>
			</a> <small><?php edit_post_link('<i class="fa fa-pencil-square" aria-hidden="true"></i>'); ?></small></h2>
	</div>
	<div class="text-hide">
		<?php get_template_part('parts/author'); ?>
	</div>
</article>
