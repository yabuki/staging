<?php
/**
 * Search results partial template.
 *
 * @package understrap
 */
// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
?>
<article <?php post_class('mb-4'); ?> id="post-<?php the_ID(); ?>">
  <div class="card card-pin">
    <?php if ( has_post_thumbnail() ): ?>
    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
      <?php the_post_thumbnail('full', array( 'class' => 'card-img' )); ?>
    </a>
    <?php else: ?>
    <a href="<?php the_permalink(); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/no-image-small-c.jpg" class="card-img"></a>
    <?php endif; ?>
    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title('<h2 class="card-title">', '</h2>'); ?></a>
    <div class="small">
      <?php 
        if (has_term('','glossary_cat')) {
		      echo '<i class="fa fa-folder-o" aria-hidden="true"></i> ';
      		the_terms( $post->ID, 'glossary_cat', '', '/' );
        }
        ?>
      <?php get_template_part('parts/entry','meta-flag'); ?>
    </div><!-- /.small -->
  </div><!-- .card-pin -->
</article><!-- #post-## -->
