<?php
/**
 * Single post partial template.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

  <?php get_template_part('parts/single-jumbotron'); ?>
  <?php if(post_custom('wpcf-previous-era1', 'wpcf-next-era1')) {
				get_template_part('parts/nav', 'era');
				}
    ?>
  <?php get_template_part('parts/alert'); ?>
  <div class="mb-2 border-bottom">
    <?php get_template_part('parts/nav', 'postlink'); ?>
  </div>

  <div class="entry-content content-single">

    <?php $parts = get_extended( $post->post_content ); ?>
    
    <?php echo wpautop( do_shortcode( $parts['main'] ) ); ?>

    <?php dynamic_sidebar( 'adsense-sidebar' ); ?>

    <div class="d-block d-sm-none g-post-widget">

      <?php if ( in_category( array( 'movie', 'dorama' ))){
  get_template_part('parts/widget', 'movie');
}
?>
    </div>

    <?php echo apply_filters( 'the_content', $parts['extended'] ); ?>

    <!-- 同時代の人物  -->
    <?php
          if(is_singular('glossary') && post_custom('wpcf-contemporaries')): 
      ?>

    <div class="alert alert-success">
      <p class="mb-1 text-dark">同時代の人物</p>
      <h4 class="alert-heading pl-0">
        <?php echo post_custom('wpcf-contemporaries'); ?></h4>

      <p><?php echo post_custom('wpcf-contemporaries-info'); ?></p>
    </div>

    <?php endif; ?>
    <!-- 同時代の人物 end -->

    <div class="d-block d-sm-none g-post-widget">
      <?php get_template_part('parts/widget','info'); ?>

    </div>

    <?php
		wp_link_pages(
			array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'sekainorekisi-map' ),
				'after'  => '</div>',
			)
		);
		?>

    <footer class="entry-footer">

      <?php edit_post_link( __( 'Edit', 'sekainorekisi-map' ), '<span class="edit-link">', '</span>' ); ?>

    </footer><!-- .entry-footer -->
  </div><!-- .entry-content -->
</article><!-- #post-## -->
