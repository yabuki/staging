<?php
/**
 * Partial template for content in single-history.php
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

  <?php get_template_part('parts/single-jumbotron'); ?>
  <?php get_template_part('parts/alert'); ?>
  <div class="mb-2 border-bottom">
    <?php get_template_part('parts/nav', 'postlink'); ?>
  </div>

  <div class="entry-content content-single-glossary mb-5">

    <?php $parts = get_extended( $post->post_content ); ?>
    <?php echo wpautop( do_shortcode( $parts['main'] ) ); ?>

    <?php dynamic_sidebar( 'adsense-sidebar' ); ?>

    <div class="d-block d-sm-none g-post-widget">

      <?php if ( has_term( 'person','glossary_cat' ) ) {
  get_template_part('parts/widget', 'profile');
}
?>
      <?php if ( has_term( 'world-heritage','glossary_cat' ) ) {
  get_template_part('parts/widget', 'heritage');
}
?>
    </div><!-- #d-sm-none# -->
  </div><!-- .entry-content -->
  <div class="entry-content content-single-glossary">

    <?php echo apply_filters( 'the_content', $parts['extended'] ); ?>

    <div class="d-block d-sm-none g-post-widget">
      <?php get_template_part('parts/widget','info'); ?>

    </div><!-- #d-sm-none# -->

    <?php
		wp_link_pages(
			array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'sekainorekisi-map' ),
				'after'  => '</div>',
			)
		);
		?>
    <!-- #wp link # -->

    <footer class="entry-footer">

      <?php edit_post_link( __( 'Edit', 'sekainorekisi-map' ), '<span class="edit-link">', '</span>' ); ?>

    </footer><!-- .entry-footer -->
  </div><!-- .entry-content -->
</article><!-- # article post# -->
