<?php
/**
 * The right sidebar containing the main widget area.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

?>

<?php dynamic_sidebar( 'adsense-sidebar' ); ?>

<?php if ( has_term( 'person','glossary_cat' ) ) {
  get_template_part('parts/widget', 'profile');
}
?>
<?php if ( has_term( 'world-heritage','glossary_cat' ) ) {
  get_template_part('parts/widget', 'heritage');
}
?>
<?php dynamic_sidebar('keyworld-sidebar'); ?>

<?php get_template_part('parts/widget','info'); ?>

<?php get_template_part('parts/widget', 'update'); ?>
