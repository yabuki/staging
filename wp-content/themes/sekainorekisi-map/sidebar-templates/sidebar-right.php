<?php
/**
 * The right sidebar containing the main widget area.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
if ( ! is_active_sidebar( 'right-sidebar' ) ) {
	return;
}
// when both sidebars turned on reduce col size to 3 from 4.
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );
?>

<?php if ( 'both' === $sidebar_pos ) : ?>
<div class="col-md-3 widget-area" id="right-sidebar" role="complementary">

  <?php else : ?>

  <div class="col-md-3 widget-area d-none d-sm-block" id="right-sidebar" role="complementary">

    <?php endif; ?>

    <?php if(is_singular()) {
        if(is_single()) {
          if (is_singular( array('japanese-history', 'world-history' ))) {
            get_template_part('sidebar', 'history');
          } elseif (is_singular('glossary')) {
            get_template_part('sidebar', 'keywords');
          } else {
            get_template_part('sidebar', 'single');
          }
        } elseif (is_page()) {
            if (is_page('timeline')) { 
              echo "<div class='d-none d-sm-block'>";
              get_template_part('sidebar', 'timeline');
              echo "</div>";
            } else {
              get_template_part('sidebar');
            }
          }
        } else {
          dynamic_sidebar( 'adsense-sidebar' );
          dynamic_sidebar( 'right-sidebar' );
        }
  ?>

  </div><!-- #right-sidebar -->
