<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ModalScrollable">
  Launch demo modal
</button>

<!-- Modal -->
<div class="modal fade modal-open" id="ModalScrollable" tabindex="-1" role="dialog" aria-labelledby="ModalScrollableTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="ModalScrollableTitle">ツールチップについて</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>歴史用語の緑色のリンクは、デスクトップではマウスオーバーで緑色のツールチップが表示され、クリックすると歴史用語詳細ページが新しいタブに表示されます。タッチデバイスでは、リンクをタップしたままにするとツールチップが表示されます。</p>
      </div>
    </div>
  </div>
</div>
