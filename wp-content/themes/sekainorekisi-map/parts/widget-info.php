<aside id="This-info-widget" class="widget widget_custom this-info">
  <h3 class="widget-title">
    <?php _e('POST INFORMATION','understrap-child'); ?>
  </h3>
  <ul>
    <!-- 年代-->
    <?php 
    $product_terms = wp_get_object_terms( $post->ID, 'age' );
    if ( ! empty( $product_terms ) ) {
      if ( ! is_wp_error( $product_terms ) ) {
        echo '<li><div class="type"><i class="fa fa-calendar" aria-hidden="true"></i>';
        echo __('年代','understrap-child');
        echo '</div><div class="value age-value">';
        foreach( $product_terms as $term ) {
          echo '<a class="waves-effect btn-flat" href="'. get_term_link( $term->slug, 'age' ) .'">' . esc_html( $term->name ) . '</a>';
        }
        echo '</div></li>';
      }
    }
    ?>
    <!-- 開始紀元年月日-->
    <div class="row">
      <div class="col">
        <div class="type"><i class="fa fa-clock-o" aria-hidden="true"></i>
          <?php _e('開始/誕生','understrap-child'); ?>
        </div>
        <div class="value">
          <?php 
          if(post_custom('wpcf-start-era')){
            echo post_custom('wpcf-start-era');
          }
          ?>
          <?php 
          if(post_custom('wpcf-start-year')){
            echo post_custom('wpcf-start-year');
          }
          ?>
        </div>
      </div>
      <div class="col">
        <!-- 終了紀元・年月日-->
        <div class="type"><i class="fa fa-history" aria-hidden="true"></i>
          <?php _e('終了/死亡','understrap-child'); ?>
        </div>
        <div class="value">
          <?php 
          if(post_custom('wpcf-end-year')) {
            echo post_custom('wpcf-end-era');
          }
          ?>
          <?php 
          if(post_custom('wpcf-end-year')) {
            echo post_custom('wpcf-end-year');
          }
          ?>
        </div>
      </div>
    </div>
    <!-- 時代-->
    <?php 
    $product_terms = wp_get_object_terms( $post->ID, 'era-name' );
    if ( ! empty( $product_terms ) ) {
      if ( ! is_wp_error( $product_terms ) ) {
        echo '<li><div class="type"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i>';
        echo __('時代','understrap-child');
        echo '</div><div class="value age-value">';
        foreach( $product_terms as $term ) {
          echo '<a class="waves-effect btn-flat" href="'. get_term_link( $term->slug, 'era-name' ) .'">' . esc_html( $term->name ) . '</a>';
        }
        echo '</div></li>';
      }
    }
    ?>
    <!-- 君主-->
    <?php 
    $product_terms = wp_get_object_terms( $post->ID, 'historypresidents' );
    if ( ! empty( $product_terms ) ) {
      if ( ! is_wp_error( $product_terms ) ) {
        echo '<li><div class="type"><i class="fa fa-user-circle-o" aria-hidden="true"></i>';
        echo __('君主','understrap-child');
        echo '</div><div class="value age-value">';
        foreach( $product_terms as $term ) {
          echo '<a class="waves-effect btn-flat" href="'. get_term_link( $term->slug, 'historypresidents' ) .'">' . esc_html( $term->name ) . '</a>';
        }
        echo '</div></li>';
      }
    }
    ?>
    <!-- 政策 -->
    <?php 
    $product_terms = wp_get_object_terms( $post->ID, 'policy' );
    if ( ! empty( $product_terms ) ) {
      if ( ! is_wp_error( $product_terms ) ) {
        echo '<li><div class="type"><i class="fa fa-tasks" aria-hidden="true"></i>';
        echo __('政策','understrap-child');
        echo '</div><div class="value age-value">';
        foreach( $product_terms as $term ) {
          echo '<a class="waves-effect btn-flat" href="'. get_term_link( $term->slug, 'policy' ) .'">' . esc_html( $term->name ) . '</a>';
        }
        echo '</div></li>';
      }
    }
    ?>
    <!-- 皇族 -->
    <?php 
    $product_terms = wp_get_object_terms( $post->ID, 'royalty' );
    if ( ! empty( $product_terms ) ) {
      if ( ! is_wp_error( $product_terms ) ) {
        echo '<li><div class="type"><i class="fa fa-users" aria-hidden="true"></i>';
        echo __('皇族','understrap-child');
        echo '</div><div class="value age-value">';
        foreach( $product_terms as $term ) {
          echo '<a class="waves-effect btn-flat" href="'. get_term_link( $term->slug, 'royalty' ) .'">' . esc_html( $term->name ) . '</a>';
        }
        echo '</div></li>';
      }
    }
    ?>
    <!-- 将軍 -->
    <?php 
    $product_terms = wp_get_object_terms( $post->ID, 'j_general' );
    if ( ! empty( $product_terms ) ) {
      if ( ! is_wp_error( $product_terms ) ) {
        echo '<li><div class="type"><i class="fa fa-user-o" aria-hidden="true"></i>';
        echo __('将軍','understrap-child');
        echo '</div><div class="value age-value">';
        foreach( $product_terms as $term ) {
          echo '<a class="waves-effect btn-flat" href="'. get_term_link( $term->slug, 'j_general' ) .'">' . esc_html( $term->name ) . '</a>';
        }
        echo '</div></li>';
      }
    }
    ?>
    <!-- 戦争 -->
    <?php 
    $product_terms = wp_get_object_terms( $post->ID, 'history_war' );
    if ( ! empty( $product_terms ) ) {
      if ( ! is_wp_error( $product_terms ) ) {
        echo '<li><div class="type"><i class="fa fa-times" aria-hidden="true"></i>';
        echo __('戦争','understrap-child');
        echo '</div><div class="value age-value">';
        foreach( $product_terms as $term ) {
          echo '<a class="waves-effect btn-flat" href="'. get_term_link( $term->slug, 'history_war' ) .'">' . esc_html( $term->name ) . '</a>';
        }
        echo '</div></li>';
      }
    }
    ?>
    <!-- 芸術 -->
    <?php 
    $product_terms = wp_get_object_terms( $post->ID, 'historyart' );
    if ( ! empty( $product_terms ) ) {
      if ( ! is_wp_error( $product_terms ) ) {
        echo '<li><div class="type"><i class="fa fa-paint-brush" aria-hidden="true"></i>';
        echo __('芸術','understrap-child');
        echo '</div><div class="value age-value">';
        foreach( $product_terms as $term ) {
          echo '<a class="waves-effect btn-flat" href="'. get_term_link( $term->slug, 'historyart' ) .'">' . esc_html( $term->name ) . '</a>';
        }
        echo '</div></li>';
      }
    }
    ?>
    <!-- 文化人 -->
    <?php 
    $product_terms = wp_get_object_terms( $post->ID, 'culture-artist' );
    if ( ! empty( $product_terms ) ) {
      if ( ! is_wp_error( $product_terms ) ) {
        echo '<li><div class="type"><i class="fa fa-user" aria-hidden="true"></i>';
        echo __('文化人','understrap-child');
        echo '</div><div class="value age-value">';
        foreach( $product_terms as $term ) {
          echo '<a class="waves-effect btn-flat" href="'. get_term_link( $term->slug, 'culture-artist' ) .'">' . esc_html( $term->name ) . '</a>';
        }
        echo '</div></li>';
      }
    }
    ?>
    <!-- 建築 -->
    <?php 
    $product_terms = wp_get_object_terms( $post->ID, 'historybuilding' );
    if ( ! empty( $product_terms ) ) {
      if ( ! is_wp_error( $product_terms ) ) {
        echo '<li><div class="type"><i class="fa fa-building-o" aria-hidden="true"></i>';
        echo __('建築','understrap-child');
        echo '</div><div class="value age-value">';
        foreach( $product_terms as $term ) {
          echo '<a class="waves-effect btn-flat" href="'. get_term_link( $term->slug, 'historybuilding' ) .'">' . esc_html( $term->name ) . '</a>';
        }
        echo '</div></li>';
      }
    }
    ?>
  </ul>
</aside>
