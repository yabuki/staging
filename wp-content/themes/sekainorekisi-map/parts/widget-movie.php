<aside class="widget widget-movie-info">
  <h3 class="widget-title"><?php _e('MOVIE INFORMATION' , 'understrap-child'); ?></h3>
  <?php if(post_custom('wpcf-amazon')): ?>
    <div class="amazon-image"><?php echo apply_filters('the_content', get_post_meta($post->ID, 'wpcf-amazon', true)); ?></div>
  <?php endif; ?>
  <dl itemscope itemtype="http://data-vocabulary.org/Review">
    <?php if(post_custom('wpcf-movie-title')): ?>
      <dt><?php _e('作品名' , 'understrap-child'); ?></dt>
      <dd itemprop="reviewer"><?php echo post_custom('wpcf-movie-title'); ?></dd>
    <?php endif; ?>
    <?php if(post_custom('wpcf-directed')): ?>
      <dt>
        <?php _e('ディレクター' , 'understrap-child'); ?>
      </dt>
      <dd>
        <?php echo post_custom('wpcf-directed'); ?>
      </dd>
    <?php endif; ?>
    <?php if(post_custom('wpcf-starring')): ?>
      <dt><?php _e('主演' , 'understrap-child'); ?></dt>
      <dd><?php echo post_custom('wpcf-starring'); ?></dd>
    <?php endif; ?>
    <?php if(post_custom('wpcf-release')): ?>
      <dt><?php _e('リリース日' , 'understrap-child'); ?></dt>
      <dd><?php echo post_custom('wpcf-release'); ?></dd>
    <?php endif; ?>
    <?php if(post_custom('wpcf-p_country')): ?>
      <dt><?php _e('製造国' , 'understrap-child'); ?></dt>
      <dd><?php echo post_custom('wpcf-p_country'); ?></dd>
    <?php endif; ?>
    <?php if(post_custom('wpcf-s_country')): ?>
      <dt><?php _e('舞台国' , 'understrap-child'); ?></dt>
      <dd><?php echo post_custom('wpcf-s_country'); ?></dd>
    <?php endif; ?>
    <?php if(post_custom('wpcf-models')): ?>
      <dt><?php _e('モデル' , 'understrap-child'); ?></dt>
      <dd><?php echo post_custom('wpcf-models'); ?></dd>
    <?php endif; ?>
    <?php if(post_custom('wpcf-winning')): ?>
      <dt><?php _e('受賞' , 'understrap-child'); ?></dt>
      <dd><?php echo post_custom('wpcf-winning'); ?></dd>
    <?php endif; ?>
    <?php if(post_custom('wpcf-reviws-date')): ?>
      <dt><?php _e('レビュー日' , 'understrap-child'); ?></dt>
      <dd><time itemprop="dtreviewed" datetime="<?php echo post_custom("wpcf-reviws-date"); ?>"><?php echo post_custom("wpcf-reviws-date"); ?></time></dd>
    <?php endif; ?>      
    <?php if(post_custom('wpcf-review')): ?>
      <dt><?php _e('レビュー' , 'understrap-child'); ?></dt>
      <dd itemprop="itemreviewed"><?php echo post_custom('wpcf-review'); ?></dd>
    <?php endif; ?>
    <?php if(post_custom('wpcf-rating')): ?>  
      <dt><?php _e('おすすめ度' , 'understrap-child'); ?></dt>
      <dd><span class="text-hide" itemprop="rating"><?php echo post_custom('wpcf-rating'); ?></span>
        <div class="rating-star" id="score-callback" data-score="<?php echo post_custom('wpcf-rating'); ?>"></div>
      </dd>
    <?php endif; ?>       
    <?php if(post_custom('wpcf-note')): ?>  
      <dt><?php _e('備考' , 'understrap-child'); ?></dt>
      <dd><?php echo post_custom('wpcf-note'); ?></dd>
    <?php endif; ?>  
  </dl>
</aside>
<script>
jQuery(function ($) {
  $('#score-callback').raty({
    readOnly: true,
    starType: 'i',
    half: true,
    halfShow: true,
    score: function () {
      return $(this).attr('data-score');
    }
  }); //end Rating
});
</script>
