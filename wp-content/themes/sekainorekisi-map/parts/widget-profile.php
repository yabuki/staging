<?php if(post_custom('wpcf-profile-on') =='ON'): ?>
<aside class="widget widget_profile">

  <h3 class="widget-title"><?php _e('PROFILE','understrap-child'); ?></h3>

  <?php if(post_custom('wpcf-title')): ?>
  <h4 class="profile-title center-align"><?php the_field('wpcf-title'); ?></h4>
  <?php endif; ?>

  <?php if(post_custom('wpcf-portrait')): ?>
  <p class="portrait-img">
    <img src="<?php echo (post_custom('wpcf-portrait', array('output' => 'raw'))); ?>" /></p>
  <?php endif; ?>

  <dl>

    <?php if(post_custom('wpcf-dynasty')): ?>
    <dt><?php _e('王朝' , 'understrap-child'); ?></dt>
    <dd><?php the_field('wpcf-dynasty'); ?>
      <?php endif; ?>

      <?php if(post_custom('wpcf-generations')): ?>
      <span class="pl-1"><?php the_field('wpcf-generations'); ?></span></dd>
    <?php endif; ?>


    <?php if(post_custom('wpcf-reign')): ?>
    <dt><?php _e('在位' , 'understrap-child'); ?></dt>
    <dd><?php the_field('wpcf-reign'); ?></dd>
    <?php endif; ?>

    <?php if(post_custom('wpcf-capital')): ?>
    <dt><?php _e('首都' , 'understrap-child'); ?></dt>
    <dd><?php the_field('wpcf-capital'); ?></dd>
    <?php endif; ?>

    <?php if(post_custom('wpcf-full_name')): ?>
    <dt><?php _e('姓名' , 'understrap-child'); ?></dt>
    <dd><?php the_field('wpcf-full_name'); ?></dd>
    <?php endif; ?>

    <?php if(post_custom('wpcf-another-name')): ?>
    <dt><?php _e('別号' , 'understrap-child'); ?></dt>
    <dd><?php the_field('wpcf-another-name'); ?></dd>
    <?php endif; ?>

    <?php if(post_custom('wpcf-royal-family')): ?>
    <dt><?php _e('王家' , 'understrap-child'); ?></dt>
    <dd><?php the_field('wpcf-royal-family'); ?></dd>
    <?php endif; ?>

    <?php if(post_custom('wpcf-born')): ?>
    <div class="row mb0">
      <div class="col-sm">
        <dt><?php _e('生誕' , 'understrap-child'); ?></dt>
        <dd><?php the_field('wpcf-born'); ?></dd>
      </div>
      <?php endif; ?>

      <?php if(post_custom('wpcf-died')): ?>
      <div class="col-sm">
        <dt><?php _e('死去' , 'understrap-child'); ?></dt>
        <dd><?php the_field('wpcf-died'); ?></dd>
      </div>
    </div>
    <?php endif; ?>

    <?php if(post_custom('wpcf-burial')): ?>
    <dt><?php _e('埋葬' , 'understrap-child'); ?></dt>
    <dd><?php the_field('wpcf-burial'); ?></dd>
    <?php endif; ?>

    <?php if(post_custom('wpcf-spouse')): ?>
    <dt><?php _e('配偶者' , 'understrap-child'); ?></dt>
    <dd><?php the_field('wpcf-spouse'); ?></dd>
    <?php endif; ?>

    <?php if(post_custom('wpcf-child')): ?>
    <dt><?php _e('子女' , 'understrap-child'); ?></dt>
    <dd><?php the_field('wpcf-child'); ?></dd>
    <?php endif; ?>

    <?php if(post_custom('wpcf-father')): ?>
    <dt><?php _e('父親' , 'understrap-child'); ?></dt>
    <dd><?php the_field('wpcf-father'); ?></dd>
    <?php endif; ?>

    <?php if(post_custom('wpcf-mother')): ?>
    <dt><?php _e('母親' , 'understrap-child'); ?></dt>
    <dd><?php the_field('wpcf-mother'); ?></dd>
    <?php endif; ?>

  </dl>
</aside>
<?php else: ?>
<?php endif; ?>
