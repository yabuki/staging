<div class="chapter-nav">
  <div class="row align-items-start">
    <div class="col-6 text-left">
      <?php previous_post_link( '<span class="meta-nav">Previous Post</span><p class="mb-0 font-weight-bold chevron-left">%link</p>', '%title' ); ?>
    </div>
    <div class="col-6 text-right">
      <?php next_post_link('<span class="meta-nav">Next Post</span><p class="text-right mb-0 font-weight-bold chevron-right">%link</p>', '%title' ); ?>
    </div>
  </div>
</div>
