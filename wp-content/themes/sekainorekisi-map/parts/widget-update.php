<aside class="widget_update widget">
  <h3 class="widget-title"><?php _e('UPDATE POSTS','understrap-child'); ?></h3>
  <?php // 更新記事
  $lat_args = array (
    'posts_per_page' => 6,
    'orderby' => 'modified',
    'post_type' => array('glossary', 'post'),
    );
  $lat_query = new WP_Query( $lat_args );
  ?>
  <div class="row">
    <?php while ( $lat_query->have_posts() ) : $lat_query->the_post(); ?>


    <div class="col-6">
      <div class="card mb-1">

        <?php if ( has_post_thumbnail() ): ?>
        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
          <?php the_post_thumbnail( 'thumbnail', array('class' => 'card-img-top') ); ?>
        </a>
        <?php else: ?>
        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
          <img src="<?php echo get_stylesheet_directory_uri() ?>/images/no-image300x300.jpg" alt="<?php the_title_attribute(); ?>" class="card-img-top">
        </a>
        <?php endif; ?>
        <div class="card-body">
          <?php echo '<p class="card-text text-truncate"><a href="' . get_permalink() . '">' . get_the_title() . '</a></p>'; ?>
        </div>
      </div>
    </div>

    <?php endwhile; ?>
  </div>
  <?php wp_reset_postdata(); ?>
</aside>
