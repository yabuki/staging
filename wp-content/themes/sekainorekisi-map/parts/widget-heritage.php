<?php if(get_post_meta($post->ID,'wpcf-s-ok',true) == 'ON'): ?>
<section class="widget widget_profile z-depth-1">
  <h3 class="widget-title"><?php _e('世界遺産情報','understrap-child'); ?></h3>

  <?php if(post_custom('wpcf-s-1')): ?>
  <h4 class="profile-title center-align"><?php echo post_custom('wpcf-s-1'); ?></h4>
  <?php endif; ?>

  <?php if(post_custom('wpcf-s-2')): ?>
  <p class="portrait-img">
    <img src="<?php echo (post_custom('wpcf-s-2', array('output' => 'raw'))); ?>" /></p>
  <?php endif; ?>

  <dl>
    <?php if(post_custom('wpcf-s-5')): ?>
    <dt><?php _e('国' , 'understrap-child'); ?></dt>
    <dd><?php echo post_custom('wpcf-s-5'); ?></dd>
    <?php endif; ?>

    <?php if(post_custom('wpcf-s-date')): ?>
    <dt><?php _e('登録日' , 'understrap-child'); ?></dt>
    <dd><?php echo post_custom('wpcf-s-date'); ?></dd>
    <?php endif; ?>

    <?php if(post_custom('wpcf-s-6')): ?>
    <dt><?php _e('宗派' , 'understrap-child'); ?></dt>
    <dd><?php echo post_custom('wpcf-s-6'); ?></dd>
    <?php endif; ?>

    <?php if(post_custom('wpcf-s-7')): ?>
    <dt><?php _e('ウェブサイト' , 'understrap-child'); ?></dt>
    <dd style="font-style:italic;"><a href="<?php echo post_custom('wpcf-s-7'); ?>" title="Web Site"><?php echo post_custom('wpcf-s-7'); ?></a></dd>
    <?php endif; ?>

    <?php if(post_custom('wpcf-s-8')): ?>
    <dt><?php _e('登録区分' , 'understrap-child'); ?></dt>
    <dd><?php echo post_custom('wpcf-s-8'); ?></dd>
    <?php endif; ?>

    <?php if(post_custom('wpcf-s-9')): ?>
    <dt><?php _e('スタイル' , 'understrap-child'); ?></dt>
    <dd><?php echo post_custom('wpcf-s-9'); ?></dd>
    <?php endif; ?>

    <?php if(post_custom('wpcf-s-10')): ?>
    <dt><?php _e('長さ' , 'understrap-child'); ?></dt>
    <dd><?php echo post_custom('wpcf-s-10'); ?></dd>
    <?php endif; ?>

    <?php if(post_custom('wpcf-s-11')): ?>
    <dt><?php _e('幅' , 'understrap-child'); ?></dt>
    <dd><?php echo post_custom('wpcf-s-11'); ?></dd>
    <?php endif; ?>

    <?php if(post_custom('wpcf-s-12')): ?>
    <dt><?php _e('高さ' , 'understrap-child'); ?></dt>
    <dd><?php echo post_custom('wpcf-s-12'); ?></dd>
    <?php endif; ?>

    <?php if(post_custom('wpcf-s-13')): ?>
    <dt><?php _e('登録名' , 'understrap-child'); ?></dt>
    <dd><?php echo post_custom('wpcf-s-13'); ?></dd>
    <?php endif; ?>

    <?php if(post_custom('wpcf-biko')): ?>
    <dt><?php _e('その他' , 'understrap-child'); ?></dt>
    <dd><?php echo post_custom('wpcf-biko'); ?></dd>
    <?php endif; ?>

  </dl>
</section>
<?php endif; ?>
