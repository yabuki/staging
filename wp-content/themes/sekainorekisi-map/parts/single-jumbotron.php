<header class="row jumbotron-mini">
  <div class="col-12 col-sm-6 mb-1 p-0">
    <figure class="figure bg-white shadow-sm mb-0 wp-caption">
      <?php 
        if ( has_post_thumbnail() ) { 
          the_post_thumbnail();
			echo '<figcaption class="figure-caption">';
			the_post_thumbnail_caption(); 
			echo '</figcaption>';
          
        } else {
          
            echo '<img src="';
            echo get_stylesheet_directory_uri() . '/images/no-image517x517.jpg" alt="">';
        }
        ?>
    </figure>
  </div>
  <div class="col-sm-6">

    <?php the_title( '<h1 class="entry-title font-weight-bold">', '</h1>' ); ?>

    <p class="chapter-icon">

      <?php 
        
        if ( in_array( get_post_type(), array( 'japanese-history', 'world-history' ) ) ) {
          if(has_term('','chapter')){
            the_terms( $post->ID, 'chapter', '', ' &gt; ' );	
          }
          
        } elseif (has_term('','glossary_cat')) {
          the_terms( $post->ID, 'glossary_cat', '', '/' );
        
        } elseif (is_singular('chronology')) {
          echo '年表';
          
        } elseif (has_term('', 'wpdmcategory')) {
          the_terms( $post->ID, 'wpdmcategory', '', ' / ', "" );
          
        } else {
          the_category(' &gt; ', 'single' );
        }
        ?>

    </p>
    <p>
      <time class="entry-date publish date updated date" datetime="<?= get_post_time('c', true); ?>" itemprop="datePublished"> <span class="published-icon"> 公開日</span>
        <?= get_the_date(); ?>
      </time>
      <time class="entry-date updated" datetime="<?= get_post_time('c', true); ?>" itemprop="dateModified"> <span class="update-icon"> 最終更新日</span>
        <?= the_modified_date(); ?>
      </time>
    </p>

    <?php if(has_term('','history_tag')){ ?>
    <p class="tag-icon">
      <?php the_terms( $post->ID, 'history_tag', '', ' / ', "" ); ?>
    </p>
    <?php } ?>

    <?php get_template_part('parts/entry-meta-flag'); ?>

  </div>
</header>
