<div class="col-12">
    <?php
    $args = array(
        'post_type' => 'infomation',
        'posts_per_page' => 1,
        );
    $query = new WP_Query($args);
    ?>
    <?php
    if ( $query->have_posts() ) : ?>
    <div class="ticker">
      <?php while ( $query->have_posts() ) : $query->the_post();?>
				 <p class="text-dark"><span class="mr-2"><i class="fa fa-info-circle text-info" aria-hidden="true"></i><?= get_the_date(); ?></span><?php the_title(); ?><?php the_content(); ?></p>
      <?php endwhile; ?>
    </div>
<?php endif; wp_reset_postdata(); ?>
</div>
