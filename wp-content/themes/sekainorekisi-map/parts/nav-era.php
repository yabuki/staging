<?php if(is_object_in_term( $post->ID, 'previous-era' )) : ?>
<div class="row era-menu mb-3 mt-2 small bg-light">
	<div class="col-6">
		<?php if(post_custom('wpcf-previous-era1')): ?>
		<p class="mb-0"><i class="fa fa-arrow-left text-info" aria-hidden="true"></i>
			<?php echo post_custom('wpcf-previous-era1'); ?></p>
		<?php endif; ?>
		<?php if(post_custom('wpcf-previous-era2')): ?>
		<p class="mb-0"><i class="fa fa-arrow-left text-info" aria-hidden="true"></i>
			<?php echo post_custom('wpcf-previous-era2'); ?></p>
		<?php endif; ?>
	</div>
	<?php endif; ?>
	<div class="col-6 text-right">
		<?php if(is_object_in_term( $post->ID, 'next-era' )) : ?>
		<?php if(post_custom('wpcf-next-era1')): ?>
		<p class="mb-0"><i class="fa fa-arrow-right text-info" aria-hidden="true"></i>
			<?php echo post_custom("wpcf-next-era1"); ?></p>
		<?php endif; ?>
		<?php if(post_custom('wpcf-next-era2')): ?>
		<p class="mb-0"><i class="fa fa-arrow-right text-info" aria-hidden="true"></i>
			<?php echo post_custom("wpcf-next-era2"); ?></p>
		<?php endif; ?>
	</div>
</div>
<?php endif; ?>
