<div class="meta-flag">
  <?php 
  $terms = wp_get_object_terms($post->ID,'country'); 
  foreach($terms as $term){ 
    echo '<a href="' . get_term_link( $term->slug, 'country' ) . '">' ;
    echo '<i class="sprite sprite-';
    echo $term->slug.''; 
    echo '"></i>' . esc_html( $term->name ) . '</a> ';
  }
  ?>
</div>