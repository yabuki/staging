<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
?>


<?php dynamic_sidebar( 'adsense-sidebar' ); ?>

<?php dynamic_sidebar( 'right-sidebar' ); ?>
