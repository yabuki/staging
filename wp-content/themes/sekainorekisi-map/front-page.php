<?php
/**
 * The template for displaying front page.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container   = get_theme_mod( 'understrap_container_type' );
?>

<?php get_template_part( 'global-templates/hero', 'none' ); ?>

<div class="wrapper" id="frontpage-wrapper">

  <div class="container-fluid" id="content" tabindex="-1">

    <div class="row single-frontpage">

      <div class="col content-area" id="primary">

        <main class="site-main container" id="main">

          <?php get_template_part('parts/infomation'); ?>

          <?php while ( have_posts() ) : the_post(); ?>

          <?php get_template_part( 'loop-templates/content', 'front' )?>
          <?php // glossary get_template_part( 'loop-templates/content', 'front-2' )?>

          <?php endwhile; // end of the loop. ?>

        </main><!-- #main -->

      </div><!-- #primary -->

    </div><!-- .row -->

  </div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>
