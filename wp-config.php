<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'R65DisnxH2pt/YQzXSBdyOa2YDmPlTk5qiJyr1ydnJ+mxPZI2pTOAjQPSOHdC298xIy95t6ro3R1HMr+Z+NXeQ==');
define('SECURE_AUTH_KEY',  '2OS5xMWgzM/6gITztrKqMuesbwsd3402conQljBuN4+LQCl5C1gKZT+asbR4Jt+M7SdQam/uroCo7Uuofd9/jQ==');
define('LOGGED_IN_KEY',    'PblrzmSc/E3PV35mDzBhbX1byfK7mVZBHwoPbjvS8NVQXFeMqUXMiH69fCYiEGLOkuRJd4QR6DxaDH3AaYCfnA==');
define('NONCE_KEY',        'UHuQpIlRziNfDdsfWXFwm+7bQTYLhHxCYlR4SBEEBRHqoZxRAk4adfUlAa3TkRtU0iyCYGZde1qZSuoTW0nAZg==');
define('AUTH_SALT',        'lMy4I3D8MJ7UIIv5k00lL6l9QxzLqOOPOGV7h7vDbNyXQzNmiE5zN60dHZTlsZoTbLJaCX3kPAomeDGLZyutAA==');
define('SECURE_AUTH_SALT', 'OAKARli1JkKKLCdzPQdukouFiSH55CtGP9WhHlhNLGoJvpDHIXfKJT+0+mHt/7J2OotHobLly5bQDh8B29CpGA==');
define('LOGGED_IN_SALT',   'G3OvnAzPX2QUA+7LLWard4WR2Aw3wsrQL6FdL86HmpqP/Nd00K2JDxQ8XFl6jq6HP2T1mRMiqTBjzT1PnTlJLQ==');
define('NONCE_SALT',       'yPWV9fGmz6kiPeiQXKIgoKFXLmW4NE+ETGXxnVHXbdasIbCFaW7AwT1mRBKHzMw5aVcaSutV02p2jEx4EcHhMA==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
